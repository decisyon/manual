﻿<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="../helpproject.xsl" ?>
<topic template="Default" lasteditedby="sdifranc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../helpproject.xsd">
  <title translate="true">Line Graphs - Design Studio Manual</title>
  <body>
    <header>
      <para styleclass="Heading1"><text styleclass="Heading1" translate="true">Line Graphs - Design Studio Manual</text></para>
    </header>
    <para styleclass="Normal_fill" style="text-align:left; text-indent:0px; margin-top:0px; margin-right:0px; margin-bottom:0px; margin-left:0px; line-height:1.00; background-color:transparent; white-space:normal; page-break-inside:auto; page-break-after:auto; border:none; tabstops:none;"></para>
    <para styleclass="Normal"><text styleclass="Normal" translate="true"> In a line graph the data is represented as a series of points joined by a line.</text></para>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">This type of graph is particularly suited to representing the data of a large number of groups (</text><text styleclass="Normal" translate="true">for</text><text styleclass="Normal" translate="true">&#32;</text><text styleclass="Normal" translate="true">example, the total sales of the last few years</text><text styleclass="Normal" translate="true">), outlining the trends by category.</text></para>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">The static line graph types can be selected in the </text><text styleclass="Normal" translate="true">graphStyle</text><text styleclass="Normal" translate="true"> property (</text><text styleclass="Normal" translate="true">Static Chart</text><text styleclass="Normal" translate="true"> section):</text></para>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">·</text><text styleclass="Normal" translate="true"> &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160;</text><text styleclass="Normal" translate="true">3D Line</text></para>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">·</text><text styleclass="Normal" translate="true"> &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160;</text><text styleclass="Normal" translate="true">Multiple line</text></para>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">·</text><text styleclass="Normal" translate="true"> &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160;</text><text styleclass="Normal" translate="true">Step line</text></para>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">·</text><text styleclass="Normal" translate="true"> &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160;</text><text styleclass="Normal" translate="true">Trend line</text></para>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">·</text><text styleclass="Normal" translate="true"> &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160;</text><text styleclass="Normal" translate="true">Spline line</text></para>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">·</text><text styleclass="Normal" translate="true"> &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160;</text><text styleclass="Normal" translate="true">Symbol Line</text></para>
    <para styleclass="Normal_msonormal" style="text-align:justify; text-indent:0px; margin-top:14px; margin-right:0px; margin-bottom:14px; margin-left:0px; line-height:1.19; background-color:transparent; white-space:normal; page-break-inside:auto; page-break-after:auto; border:none; tabstops:none;"><image src="img836.jpg" scale="100.00%" styleclass="Image Caption"></image></para>
    <para styleclass="Normal_msonormal" style="text-align:justify; text-indent:0px; margin-top:14px; margin-right:0px; margin-bottom:0px; margin-left:0px; line-height:1.19; background-color:transparent; white-space:normal; page-break-inside:auto; page-break-after:auto; border:none; tabstops:none;"></para>
  </body>
</topic>
