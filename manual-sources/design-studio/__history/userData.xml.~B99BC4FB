﻿<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="../helpproject.xsl" ?>
<topic template="Default" lasteditedby="sdifranc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../helpproject.xsd">
  <title translate="true">User Data - Design Studio Manual</title>
  <body>
    <header>
      <para styleclass="Heading1"><text styleclass="Heading1" translate="true">User Data</text></para>
    </header>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">You must enter user data in the </text><text styleclass="Normal" translate="true">User</text><text styleclass="Normal" translate="true">&#32;</text><text styleclass="Normal" translate="true">Data </text><text styleclass="Normal" translate="true">folder which includes the following fields (see the figure below):</text></para>
    <para styleclass="Normal"><image src="img85.png" scale="100.00%" styleclass="Normal"></image></para>
    <para styleclass="Normal"></para>
    <list id="0" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Normal" style="font-family:Symbol; font-size:11pt; color:#000000;">
      <li styleclass="Normal"><text styleclass="Normal" translate="true">&#32;</text><text styleclass="Normal" translate="true">Last Name </text><text styleclass="Normal" translate="true">and</text><text styleclass="Normal" translate="true"> Name: </text><text styleclass="Normal" translate="true">of the user</text></li>
    </list>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">This information is displayed both in this user management window and in the DAC welcome message.</text></para>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">The default order is </text><text styleclass="Normal" translate="true">Surname</text><text styleclass="Normal" translate="true">, </text><text styleclass="Normal" translate="true">Name, </text><text styleclass="Normal" translate="true">but it can be reversed using the </text><text styleclass="Normal" translate="true">subject-fullname </text><text styleclass="Normal" translate="true">option, which can be &#160;accessed from the menu item:</text></para>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">Tools&gt;&gt; Properties … &gt;&gt; Main, Users </text><text styleclass="Normal" translate="true">folder</text></para>
    <list id="1" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Normal" style="font-family:Symbol; font-size:11pt; color:#000000;">
      <li styleclass="Normal"><text styleclass="Normal" translate="true">&#32;</text><text styleclass="Normal" translate="true">User Name: </text><text styleclass="Normal" translate="true">user name is required as an access credential.</text><text styleclass="Normal" translate="true">&#32;</text><text styleclass="Normal" translate="true">Special characters can be entered (e.g. £ % &amp; [ ] …) with the exception of spaces, superscript characters , double superscript characters</text><text styleclass="Normal" translate="true">&#32;</text><text styleclass="Normal" translate="true"> and round brackets </text><text styleclass="Normal" translate="true">()</text></li>
    </list>
    <list id="2" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Normal" style="font-family:Symbol; font-size:11pt; color:#000000;">
      <li styleclass="Normal"><text styleclass="Normal" translate="true">&#32;</text><text styleclass="Normal" translate="true">E-Mail: </text><text styleclass="Normal" translate="true">user&apos;s e-mail address. A valid e-mail address must be provided, in the account@domain format. The e-mail address, if specified, is used by the system for </text><text styleclass="Normal" translate="true">e-mail notification</text><text styleclass="Normal" translate="true"> services</text></li>
    </list>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">You cannot enter an e-mail address associated with another user. In the case of duplicate e-mail addresses you will receive an alert.</text></para>
    <list id="3" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Normal" style="font-family:Symbol; font-size:11pt; color:#000000;">
      <li styleclass="Normal"><text styleclass="Normal" translate="true">Phone Number</text><text styleclass="Normal" translate="true">: a valid telephone number</text></li>
    </list>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">If you do not select the </text><text styleclass="Normal" translate="true">User has administrator privileges </text><text styleclass="Normal" translate="true">the new user is a final User with no admin grants.</text></para>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">If you leave the flag disables the user will be temporarily disabled.</text></para>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">&#32;</text></para>
  </body>
</topic>
