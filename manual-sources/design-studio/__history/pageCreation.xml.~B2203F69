﻿<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="../helpproject.xsl" ?>
<topic template="Default" lasteditedby="sdifranc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../helpproject.xsd">
  <title translate="true">Page Creation - Design Studio Manual</title>
  <body>
    <header>
      <para styleclass="Heading1"><text styleclass="Heading1" translate="true">Page Creation </text></para>
    </header>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">To create a new Page press the </text><text styleclass="Normal" translate="true">New </text><text styleclass="Normal" translate="true">button. From the </text><text styleclass="Normal" translate="true">Insert page name</text><text styleclass="Normal" translate="true"> property, assign the name to the page you are creating, while in the </text><text styleclass="Normal" translate="true">Select</text><text styleclass="Normal" translate="true">&#32;</text><text styleclass="Normal" translate="true">Template Style</text><text styleclass="Normal" translate="true"> section the system displays a gallery of templates that may be used as a starting structure for the creation of a new page.</text></para>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">&#32;</text></para>
    <para styleclass="Normal"><image src="img305.jpg" scale="100.00%" styleclass="Normal"></image></para>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">&#32;</text></para>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">Selecting the </text><text styleclass="Normal" translate="true">Empty</text><text styleclass="Normal" translate="true"> template lets you create a page without the row and column structure already predefined in the page.</text></para>
    <para styleclass="Normal"></para>
    <para styleclass="Title Example"><text styleclass="Title Example" translate="true">Example: Creating a New Page Via Template</text></para>
    <para styleclass="Title Example"><text styleclass="Title Example" translate="true">Step 1: </text><text styleclass="Title Example" translate="true">Example of creating a new page via template.</text></para>
    <para styleclass="Normal_msonormal" style="text-align:justify; text-indent:0px; margin-top:14px; margin-right:0px; margin-bottom:0px; margin-left:0px; line-height:1.19; background-color:transparent; white-space:normal; page-break-inside:auto; page-break-after:auto; border:none; tabstops:none;"><image src="img306.png" scale="100.00%" styleclass="Image Caption"></image></para>
    <para styleclass="Text Example"><text styleclass="Text Example" translate="true">Access Page Designer</text></para>
    <para styleclass="Text Example"><text styleclass="Text Example" translate="true">1. Select the Not-published folder.</text></para>
    <para styleclass="Text Example"><text styleclass="Text Example" translate="true">2. Press the New button</text></para>
    <para styleclass="Text Example"><text styleclass="Text Example" translate="true">3. In the template selection window, enter the name of the new page.</text></para>
    <para styleclass="Text Example"><text styleclass="Text Example" translate="true">4. Select the template to use as base structure. In the example, template 3 box 1 Row has been chosen.</text></para>
    <para styleclass="Text Example"></para>
    <para styleclass="Title Example"><text styleclass="Title Example" translate="true">Step </text><text styleclass="Title Example" translate="true">2: </text><text styleclass="Title Example" translate="true">Template Structure Visualization</text></para>
    <para styleclass="Normal_msonormal" style="text-align:justify; text-indent:0px; margin-top:14px; margin-right:0px; margin-bottom:0px; margin-left:0px; line-height:1.38; background-color:transparent; white-space:normal; page-break-inside:auto; page-break-after:auto; border:none; tabstops:none;"><image src="img307.jpg" scale="100.00%" styleclass="Image Caption"></image></para>
    <para styleclass="Text Example"><text styleclass="Text Example" translate="true">Selecting template “3 box 1 Row” will have the structure shown in the figure, to which additional row containers and column containers can be added.</text></para>
    <para styleclass="Normal_msonormal" style="text-align:justify; text-indent:0px; margin-top:14px; margin-right:0px; margin-bottom:0px; margin-left:0px; line-height:1.19; background-color:transparent; white-space:normal; page-break-inside:auto; page-break-after:auto; border:none; tabstops:none;"><text styleclass="s1" style="font-family:Calibri; font-size:12pt; font-weight:normal; font-style:normal; text-decoration:none; text-transform:none; vertical-align:baseline; color:#666666; background-color:transparent; letter-spacing:normal; letter-scaling:100%;" translate="true">&#32;</text></para>
  </body>
</topic>
