﻿<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="../helpproject.xsl" ?>
<topic template="Default" lasteditedby="sdifranc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../helpproject.xsd">
  <title translate="true">Widget Structure - Design Studio Manual</title>
  <body>
    <header>
      <para styleclass="Heading1"><text styleclass="Heading1" translate="true">Widget Structure</text></para>
    </header>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">· &#160; &#160; &#160; &#160; Create a directive named “helloCurrentUser”, a controller to create the User model which can be used in our template.</text></para>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">· &#160; &#160; &#160; &#160; We will use the following DAC APIs:</text></para>
    <para styleclass="Normal" style="margin-left:24px;"><text styleclass="Normal" translate="true">o &#160; the service dcyService by injecting them in our controller function arguments</text></para>
    <para styleclass="Normal" style="margin-left:24px;"><text styleclass="Normal" translate="true">o &#160; the filter dcyTranslate for translating the greeting</text></para>
    <para styleclass="Normal" style="margin-left:24px;"><text styleclass="Normal" translate="true">o &#160; the method DECISYON.ng.register.directive to register a directive in DAC</text></para>
    <para styleclass="Normal" style="margin-left:24px;"><text styleclass="Normal" translate="true">o &#160; the method DECISYON.ng.register.controller to register a controller in DAC</text></para>
    <para styleclass="Title Example"><text styleclass="Title Example" translate="true"> Custom Settings</text></para>
    <para styleclass="Normal_msonormal" style="text-align:justify; text-indent:6px; margin-top:0px; margin-right:0px; margin-bottom:0px; margin-left:0px; line-height:1.19; background-color:transparent; white-space:normal; page-break-inside:auto; page-break-after:auto; border:none; tabstops:none;"><image src="img501.jpg" scale="100.00%" styleclass="Image Caption"></image></para>
    <para styleclass="Normal_msonormal" style="text-align:justify; text-indent:6px; margin-top:0px; margin-right:0px; margin-bottom:0px; margin-left:0px; line-height:1.19; background-color:transparent; white-space:normal; page-break-inside:auto; page-break-after:auto; border:none; tabstops:none;"></para>
    <para styleclass="Normal"><text styleclass="Normal" style="font-weight:bold;" translate="true">Controller</text></para>
    <para styleclass="Normal"></para>
    <para styleclass="Normal_msonormal" style="text-align:justify; text-indent:6px; margin-top:0px; margin-right:0px; margin-bottom:0px; margin-left:0px; line-height:1.19; background-color:transparent; white-space:normal; page-break-inside:auto; page-break-after:auto; border:none; tabstops:none;"><image src="img477.png" scale="100.00%" styleclass="Image Caption"></image></para>
    <para styleclass="Normal_msonormal" style="text-align:justify; text-indent:6px; margin-top:0px; margin-right:0px; margin-bottom:0px; margin-left:0px; line-height:1.19; background-color:transparent; white-space:normal; page-break-inside:auto; page-break-after:auto; border:none; tabstops:none;"></para>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">· &#160; &#160; &#160; &#160; Define a controller to create the User model which can be used in our template. The User information data is already available in $scope variable: $scope.DECISYON.userInfo.</text></para>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">· &#160; &#160; &#160; &#160; Create and register the controller by DECISYON.ng.register.controller(‘nameController’, ConstructorController). The function passed is injected with the services that we need.</text></para>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">· &#160; &#160; &#160; &#160; Use the DAC service dcyService to translate the label for the greeting into multiple languages.</text></para>
    <para styleclass="Normal"></para>
    <para styleclass="Normal"><text styleclass="Normal" style="font-weight:bold;" translate="true">Directive</text></para>
    <para styleclass="Normal"><image src="img478.png" scale="100.00%" styleclass="Normal"></image></para>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">&#32;</text></para>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">· &#160; &#160; &#160; &#160; Define the directive &quot;helloCurrentUser&quot;.</text></para>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">· &#160; &#160; &#160; &#160; Create and register the directive using DECISYON.ng.register.directive(‘nameDirective’, ConstructorDirective).</text></para>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">· &#160; &#160; &#160; &#160; The function passed returns a directive definition object which contains the following properties:</text></para>
    <para styleclass="Normal" style="text-indent:-24px; margin-left:81px;"><text styleclass="Normal" translate="true">o &#160; restrict: &apos;E&apos; - this means that the directive will only work as an Element.</text></para>
    <para styleclass="Normal" style="text-indent:-24px; margin-left:81px;"><text styleclass="Normal" translate="true">o &#160; template - this specifies the HTML markup that will be produced when the directive is compiled and linked by Angular. This includes HTML, data binding expressions ({{ }}), using the filter dcyTranslate.</text></para>
    <para styleclass="Normal" style="margin-left:58px;"><text styleclass="Normal" translate="true">o &#160; controller - this defines the controller that will be associated with the directive template.</text></para>
    <para styleclass="Normal" style="margin-left:58px;"><text styleclass="Normal" translate="true">o &#160; controllerAs - this defines the controller alias used in the controller code and in the view.</text></para>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">· &#160; &#160; &#160; &#160; binDSoController: true - this ensure that properties are bound to the controller instead of the scope. In combination with controllerAs, this lets us access the user variable as currUserCtrl.userInfo.fullname within the view.</text></para>
    <para styleclass="Normal"></para>
    <para styleclass="Normal"><text styleclass="Normal" style="font-weight:bold;" translate="true">View</text></para>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">&#32;</text></para>
    <para styleclass="Normal"><image src="img479.png" scale="100.00%" styleclass="Normal"></image></para>
    <para styleclass="Normal"><image src="img502.png" scale="100.00%" styleclass="Normal"></image></para>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">&#32;</text></para>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">· &#160; &#160; &#160; &#160; Define the view in index.html.</text></para>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">· &#160; &#160; &#160; &#160; Use the CSS class defined in index.css (resolved as a widget dependency).</text></para>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">· &#160; &#160; &#160; &#160; Use the directive helloCurrentUser as an element</text></para>
    <para styleclass="Normal_msonormal" style="text-align:justify; text-indent:6px; margin-top:14px; margin-right:0px; margin-bottom:0px; margin-left:0px; line-height:1.19; background-color:transparent; white-space:normal; page-break-inside:auto; page-break-after:auto; border:none; tabstops:none;"><text styleclass="s225" style="font-family:&apos;Times New Roman&apos;; font-size:12pt; font-weight:normal; font-style:normal; text-decoration:none; text-transform:none; vertical-align:baseline; color:#666666; background-color:transparent; letter-spacing:normal; letter-scaling:100%;" translate="true">&#32;</text></para>
    <para styleclass="Title Example"><text styleclass="Title Example" translate="true"> Output</text></para>
    <para styleclass="Normal_msonormal" style="text-align:justify; text-indent:6px; margin-top:0px; margin-right:0px; margin-bottom:0px; margin-left:0px; line-height:1.19; background-color:transparent; white-space:normal; page-break-inside:auto; page-break-after:auto; border:none; tabstops:none;"><text styleclass="s225" style="font-family:&apos;Times New Roman&apos;; font-size:12pt; font-weight:normal; font-style:normal; text-decoration:none; text-transform:none; vertical-align:baseline; color:#666666; background-color:transparent; letter-spacing:normal; letter-scaling:100%;" translate="true">&#32;</text></para>
    <para styleclass="Normal_msonormal" style="text-align:justify; text-indent:6px; margin-top:14px; margin-right:0px; margin-bottom:0px; margin-left:0px; line-height:1.19; background-color:transparent; white-space:normal; page-break-inside:auto; page-break-after:auto; border:none; tabstops:none;"><image src="img503.png" scale="100.00%" styleclass="Image Caption"></image></para>
    <para styleclass="Normal_msonormal" style="text-align:justify; text-indent:6px; margin-top:14px; margin-right:0px; margin-bottom:0px; margin-left:0px; line-height:1.19; background-color:transparent; white-space:normal; page-break-inside:auto; page-break-after:auto; border:none; tabstops:none;"></para>
    <para styleclass="Title Example"><text styleclass="Title Example" translate="true">Use the Widget in Page Designer</text></para>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">This screenshot shows how to use the widget in Page Designer to create a new page in DAC.</text></para>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">&#32;</text></para>
    <para styleclass="Normal"><image src="img504.png" scale="100.00%" styleclass="Normal"></image></para>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">&#32;</text></para>
    <para styleclass="Normal"><text styleclass="Normal" style="font-weight:bold;" translate="true">Note:</text><text styleclass="Normal" translate="true"> The data presentation widget shows the information of the associated report on the page (it shows the report title). In the event of non-association, the widget on the page shows a message that explains the reason for the issue.</text></para>
    <para styleclass="Normal_body-tool" style="text-align:right; text-indent:0px; margin-top:14px; margin-right:0px; margin-bottom:0px; margin-left:0px; line-height:1.00; background-color:transparent; white-space:normal; page-break-inside:auto; page-break-after:auto; border:none; tabstops:none;"></para>
  </body>
</topic>
