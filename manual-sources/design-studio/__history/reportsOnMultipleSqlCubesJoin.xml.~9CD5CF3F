﻿<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="../helpproject.xsl" ?>
<topic template="Default" lasteditedby="sdifranc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../helpproject.xsd">
  <title translate="true">Reports on multiple SQL Cubes (Join) - Design Studio Manual</title>
  <body>
    <header>
      <para styleclass="Heading1"><text styleclass="Heading1" translate="true">Reports on multiple SQL Cubes (Join) - Design Studio Manual</text></para>
    </header>
    <para styleclass="Normal_fill" style="text-align:left; text-indent:0px; margin-top:0px; margin-right:0px; margin-bottom:0px; margin-left:0px; line-height:1.00; background-color:transparent; white-space:normal; page-break-inside:auto; page-break-after:auto; border:none; tabstops:none;"></para>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">A more complex application of the query feature is to define multiple cubes of this type.</text></para>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">In this case, you need to define the join criteria for relating the two cubes:</text></para>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">· &#160; &#160; &#160; &#160; The relationship between the two cubes is defined by assigning the same alias name to the join fields, in the two queries.</text></para>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">We’ll use the sales table and the budget table, linked by the product, as an example.</text></para>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">In defining both select queries, you must assign the same ALIAS NAME to the product field:</text></para>
    <para styleclass="Normal"><table rowcount="1" colcount="2" style="width:auto; cell-padding:0px; cell-spacing:0px; page-break-inside:auto; border-width:0px; border-spacing:0px; cell-border-width:0px; border-style:none; background-color:none; head-row-background-color:none; alt-row-background-color:none;">
      <tr style="vertical-align:top">
        <td style="width:325px; height:112px;">
          <para styleclass="Normal"><text styleclass="Normal" translate="true">Query 1:</text></para>
          <para styleclass="Normal"><text styleclass="Normal" translate="true">select</text></para>
          <para styleclass="Normal"><text styleclass="Normal" translate="true">ID_CUSTOMER &#160;as &#160;ID_CUSTOMER,</text></para>
          <para styleclass="Normal"><text styleclass="Normal" translate="true"> &#160; &#160; &#160;ID_PRODUCT &#160;as &#160;ID_PRODUCT,</text></para>
          <para styleclass="Normal"><text styleclass="Normal" translate="true">SALES_QTY &#160;as &#160;SALES_QTY,</text></para>
          <para styleclass="Normal"><text styleclass="Normal" translate="true">from FT_SALES</text></para>
        </td>
        <td style="width:325px; height:112px;">
          <para styleclass="Normal"><text styleclass="Normal" translate="true">Query 2:</text></para>
          <para styleclass="Normal"><text styleclass="Normal" translate="true">select</text></para>
          <para styleclass="Normal"><text styleclass="Normal" translate="true">BDG_SALES_QTY &#160;as &#160;BDG_ _QTY,</text></para>
          <para styleclass="Normal"><text styleclass="Normal" translate="true">BDG_SALES_VAL &#160;as &#160;BDG_VAL,</text></para>
          <para styleclass="Normal"><text styleclass="Normal" translate="true"> &#160; &#160; &#160;ID_PRODUCT &#160;as &#160;ID_PRODUCT</text></para>
          <para styleclass="Normal"><text styleclass="Normal" translate="true">from FT_BUDGET</text></para>
        </td>
      </tr>
    </table></para>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">The next example details the definition of two QUERY cubes and the final result is presented</text></para>
    <para styleclass="Title Example"><text styleclass="Title Example" translate="true">&#32;</text></para>
    <para styleclass="Title Example"><text styleclass="Title Example" translate="true">Step 1: Definition of the queries for the two cubes (same ALIAS on the JOIN fields)</text></para>
    <para styleclass="Normal_msonormal" style="text-align:justify; text-indent:0px; margin-top:14px; margin-right:0px; margin-bottom:0px; margin-left:0px; line-height:1.19; background-color:transparent; white-space:normal; page-break-inside:auto; page-break-after:auto; border:none; tabstops:none;"><image src="img821.jpg" scale="100.00%" styleclass="Image Caption" style="font-size:11pt;"></image></para>
    <para styleclass="Normal_msonormal" style="text-align:justify; text-indent:0px; margin-top:14px; margin-right:0px; margin-bottom:0px; margin-left:0px; line-height:1.19; background-color:transparent; white-space:normal; page-break-inside:auto; page-break-after:auto; border:none; tabstops:none;"></para>
    <para styleclass="Text Example"><text styleclass="Text Example" style="font-style:normal;" translate="true">Two SQL queries are defined: for loading sales data and budget data:</text></para>
    <para styleclass="Text Example"><text styleclass="Text Example" style="font-style:normal;" translate="true">· &#160; &#160; &#160; &#160; both queries have the same alias for the product field.</text></para>
    <para styleclass="Text Example"><text styleclass="Text Example" style="font-style:normal;" translate="true">You map the respective metrics on both queries, using the button shown in the figure. You rename the cubes SALES and BUDGET respectively.</text></para>
    <para styleclass="Normal_msonormal" style="text-align:justify; text-indent:0px; margin-top:14px; margin-right:0px; margin-bottom:0px; margin-left:0px; line-height:1.00; background-color:transparent; white-space:normal; page-break-inside:auto; page-break-after:auto; border:none; tabstops:none;"><text styleclass="s197" style="font-family:Calibri; font-size:11pt; font-weight:normal; font-style:normal; text-decoration:none; text-transform:none; vertical-align:baseline; color:#666666; background-color:transparent; letter-spacing:normal; letter-scaling:100%;" translate="true">&#32;</text></para>
    <para styleclass="Normal_msonormal" style="text-align:justify; text-indent:0px; margin-top:14px; margin-right:0px; margin-bottom:0px; margin-left:0px; line-height:1.00; background-color:transparent; white-space:normal; page-break-inside:auto; page-break-after:auto; border:none; tabstops:none;"><text styleclass="s197" style="font-family:Calibri; font-size:11pt; font-weight:normal; font-style:normal; text-decoration:none; text-transform:none; vertical-align:baseline; color:#666666; background-color:transparent; letter-spacing:normal; letter-scaling:100%;" translate="true">&#32;</text><text styleclass="s512" style="font-family:Calibri; font-size:11pt; font-weight:bold; font-style:normal; text-decoration:none; text-transform:none; vertical-align:baseline; color:#000000; background-color:transparent; letter-spacing:normal; letter-scaling:100%;" translate="true">&#32;</text></para>
    <para styleclass="Title Example"><text styleclass="Title Example" translate="true">Step 2: Creating QUERY cubes and reports in MULTICUBE ANALYSIS</text></para>
    <para styleclass="Normal_msonormal" style="text-align:justify; text-indent:0px; margin-top:14px; margin-right:0px; margin-bottom:0px; margin-left:0px; line-height:1.19; background-color:transparent; white-space:normal; page-break-inside:auto; page-break-after:auto; border:none; tabstops:none;"><image src="img822.jpg" scale="100.00%" styleclass="Image Caption" style="font-size:11pt;"></image></para>
    <para styleclass="Normal_msonormal" style="text-align:justify; text-indent:0px; margin-top:14px; margin-right:0px; margin-bottom:0px; margin-left:0px; line-height:1.19; background-color:transparent; white-space:normal; page-break-inside:auto; page-break-after:auto; border:none; tabstops:none;"></para>
    <para styleclass="Text Example"><text styleclass="Text Example" style="font-style:normal;" translate="true">Selecting the OLAP folder creates two SQL cubes: BUDGET and SALES, with the metrics and the levels set in the SQL section.</text></para>
    <para styleclass="Text Example"><text styleclass="Text Example" style="font-style:normal;" translate="true">· &#160; &#160; &#160; &#160; You select the levels and the newly created cube metrics for creating the report.</text></para>
    <para styleclass="Text Example"><text styleclass="Text Example" style="font-style:normal;" translate="true">If you access the Query viewer window (SLQ button shown in the figure), you’ll see the join performed by the report on the two cubes, in relation to the product column.</text></para>
    <para styleclass="Text Example"><text styleclass="Text Example" style="font-style:normal;" translate="true">By running the report you get the result shown in the figure (bottom).</text></para>
    <para styleclass="Normal_msonormal" style="text-align:justify; text-indent:0px; margin-top:14px; margin-right:0px; margin-bottom:0px; margin-left:0px; line-height:1.00; background-color:transparent; white-space:normal; page-break-inside:auto; page-break-after:auto; border:none; tabstops:none;"><text styleclass="s197" style="font-family:Calibri; font-size:11pt; font-weight:bold; font-style:normal; text-decoration:none; text-transform:none; vertical-align:baseline; color:#666666; background-color:transparent; letter-spacing:normal; letter-scaling:100%;" translate="true">&#32;</text></para>
  </body>
</topic>
