﻿<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="../helpproject.xsl" ?>
<topic template="Default" lasteditedby="sdifranc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../helpproject.xsd">
  <title translate="true">The Toolbar - Design Studio Manual</title>
  <body>
    <header>
      <para styleclass="Heading1"><text styleclass="Heading1" translate="true">The Toolbar</text></para>
    </header>
    <para styleclass="Normal_fill" style="text-align:left; text-indent:0px; margin-top:0px; margin-right:0px; margin-bottom:0px; margin-left:0px; line-height:1.00; background-color:transparent; white-space:normal; page-break-inside:auto; page-break-after:auto; border:none; tabstops:none;"></para>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">The DAC-DS toolbar is shown in the following figure:</text></para>
    <para styleclass="Normal"><image src="img19.jpg" scale="100.00%" styleclass="Normal"></image></para>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">Once the</text><text styleclass="Normal" translate="true"> application is loaded, </text><link displaytype="text" defaultstyle="true" type="topiclink" href="startAndOpenDacLocal" anchor="_Accesso_al_modello_1" styleclass="Normal" translate="true">a first</link><text styleclass="Normal" translate="true"> line toolbar is enabled. Once a report is opened, two more toolbar lines are enabled, which show the functions that can be applied to the report.</text></para>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">As the mouse moves over a button, the tooltip is enabled, providing a short description of the associated function as described below:</text></para>
    <para styleclass="Normal"><table rowcount="19" colcount="3" style="width:auto; cell-padding:0px; cell-spacing:0px; page-break-inside:auto; border-width:0px; border-spacing:0px; cell-border-width:0px; border-style:none; background-color:none; head-row-background-color:none; alt-row-background-color:none;">
      <tr style="vertical-align:top">
        <td style="width:157px; height:32px;">
          <para styleclass="Normal"><text styleclass="Normal" translate="true">&#32;</text></para>
        </td>
        <td colspan="2" style="vertical-align:middle; width:488px; height:32px;">
          <para styleclass="Normal"><text styleclass="Normal" translate="true">&#32;</text></para>
        </td>
      </tr>
      <tr style="vertical-align:top">
        <td style="width:157px; height:24px;">
          <para styleclass="Normal"><image src="img20.jpg" scale="100.00%" styleclass="Normal"></image></para>
        </td>
        <td style="vertical-align:middle; width:488px; height:24px;">
          <para styleclass="Normal"><text styleclass="Normal" translate="true">Open report, Save, Save as and New report</text><text styleclass="Normal" translate="true"> (see </text><link displaytype="text" defaultstyle="true" type="topiclink" href="report" anchor="_Filtri" styleclass="Normal" translate="true">Report</link><link displaytype="text" defaultstyle="true" type="topiclink" href="report" anchor="_Filtri" styleclass="Normal" translate="true">)</link></para>
        </td>
        <td style="vertical-align:middle; width:3px;">
          <para styleclass="Normal"><text styleclass="Normal" translate="true">&#32;</text></para>
        </td>
      </tr>
      <tr style="vertical-align:top">
        <td style="width:157px; height:24px;">
          <para styleclass="Normal"><image src="img21.jpg" scale="100.00%" styleclass="Normal"></image></para>
        </td>
        <td colspan="2" style="vertical-align:middle; width:488px; height:24px;">
          <para styleclass="Normal"><text styleclass="Normal" translate="true">Dimension management</text><text styleclass="Normal" translate="true"> (see </text><link displaytype="text" defaultstyle="true" type="topiclink" href="dimensions" anchor="_Dimensioni_1" styleclass="Normal" translate="true">Dimensions</link><text styleclass="Normal" translate="true">)</text></para>
        </td>
      </tr>
      <tr style="vertical-align:top">
        <td style="width:157px; height:24px;">
          <para styleclass="Normal"><image src="img22.jpg" scale="100.00%" styleclass="Normal"></image></para>
        </td>
        <td colspan="2" style="vertical-align:middle; width:488px; height:24px;">
          <para styleclass="Normal"><text styleclass="Normal" translate="true">Cube management</text><text styleclass="Normal" translate="true"> (see </text><link displaytype="text" defaultstyle="true" type="topiclink" href="cubesAndMeasures" anchor="_Cubi_e_misure_1" styleclass="Normal" translate="true">Cubes and measures section)</link></para>
        </td>
      </tr>
      <tr style="vertical-align:top">
        <td style="width:157px; height:24px;">
          <para styleclass="Normal"><image src="img23.jpg" scale="100.00%" styleclass="Normal"></image></para>
        </td>
        <td colspan="2" style="vertical-align:middle; width:488px; height:24px;">
          <para styleclass="Normal"><text styleclass="Normal" translate="true">Metric Management</text><text styleclass="Normal" translate="true">&#32;</text><text styleclass="Normal" translate="true">(see </text><link displaytype="text" defaultstyle="true" type="topiclink" href="metrics" anchor="_Metrics" styleclass="Normal" translate="true">Metrics</link><text styleclass="Normal" translate="true">)</text></para>
        </td>
      </tr>
      <tr style="vertical-align:top">
        <td style="width:157px; height:24px;">
          <para styleclass="Normal"><image src="img24.jpg" scale="100.00%" styleclass="Normal"></image></para>
        </td>
        <td colspan="2" style="vertical-align:middle; width:488px; height:24px;">
          <para styleclass="Normal"><text styleclass="Normal" translate="true">Direct filters management</text><text styleclass="Normal" translate="true">&#32;</text><text styleclass="Normal" translate="true">(see </text><link displaytype="text" defaultstyle="true" type="topiclink" href="filters" anchor="_Direct_Filters" styleclass="Normal" translate="true">Direct Filters</link><text styleclass="Normal" translate="true">)</text></para>
        </td>
      </tr>
      <tr style="vertical-align:top">
        <td style="width:157px; height:24px;">
          <para styleclass="Normal"><image src="img25.jpg" scale="100.00%" styleclass="Normal"></image></para>
        </td>
        <td colspan="2" style="vertical-align:middle; width:488px; height:24px;">
          <para styleclass="Normal"><text styleclass="Normal" translate="true">Range filters management</text><text styleclass="Normal" translate="true">&#32;</text><text styleclass="Normal" translate="true">(see </text><link displaytype="text" defaultstyle="true" type="topiclink" href="rangeFilters" anchor="_Range_Filters" styleclass="Normal" translate="true">Range filters</link><text styleclass="Normal" translate="true">)</text></para>
        </td>
      </tr>
      <tr style="vertical-align:top">
        <td style="width:157px; height:24px;">
          <para styleclass="Normal"><image src="img26.jpg" scale="100.00%" styleclass="Normal"></image></para>
        </td>
        <td colspan="2" style="vertical-align:middle; width:488px; height:24px;">
          <para styleclass="Normal"><text styleclass="Normal" translate="true">User filters</text><text styleclass="Normal" translate="true">&#32;</text><text styleclass="Normal" translate="true">(see </text><link displaytype="text" defaultstyle="true" type="topiclink" href="userFiltersDataPrivileges" anchor="_User_Filters_Data" styleclass="Normal" translate="true">User filters</link><text styleclass="Normal" translate="true">)</text></para>
        </td>
      </tr>
      <tr style="vertical-align:top">
        <td style="width:157px; height:24px;">
          <para styleclass="Normal"><image src="img27.png" scale="100.00%" styleclass="Normal"></image></para>
        </td>
        <td colspan="2" style="vertical-align:middle; width:488px; height:24px;">
          <para styleclass="Normal"><text styleclass="Normal" translate="true">Indicator Designer</text><text styleclass="Normal" translate="true">&#32;</text><text styleclass="Normal" translate="true">(see </text><link displaytype="text" defaultstyle="true" type="topiclink" href="indicatorDesigner" anchor="_Indicator_designer_2" styleclass="Normal" translate="true">Indicator Designer</link><text styleclass="Normal" translate="true">)</text></para>
        </td>
      </tr>
      <tr style="vertical-align:top">
        <td style="width:157px; height:24px;">
          <para styleclass="Normal"><image src="img28.png" scale="100.00%" styleclass="Normal"></image></para>
        </td>
        <td colspan="2" style="vertical-align:middle; width:488px; height:24px;">
          <para styleclass="Normal"><text styleclass="Normal" translate="true">Page Designer</text><text styleclass="Normal" translate="true">&#32;</text><text styleclass="Normal" translate="true">(see </text><link displaytype="text" defaultstyle="true" type="topiclink" href="dacDesignPage" anchor="_DAC_Design_Page" styleclass="Normal" translate="true">DAC Design page</link><text styleclass="Normal" translate="true">)</text></para>
        </td>
      </tr>
      <tr style="vertical-align:top">
        <td style="width:157px; height:63px;">
          <para styleclass="Normal"><image src="img29.png" scale="100.00%" styleclass="Normal"></image></para>
        </td>
        <td colspan="2" style="vertical-align:middle; width:488px; height:63px;">
          <para styleclass="Normal"><text styleclass="Normal" translate="true">Scheduler, Notification Designer, Rule setup</text><text styleclass="Normal" translate="true">&#32;</text><text styleclass="Normal" translate="true">(see</text><text styleclass="Normal" translate="true">&#32;</text><link displaytype="text" defaultstyle="true" type="topiclink" href="schedule" anchor="_Schedule" styleclass="Normal" translate="true">Scheduler</link><text styleclass="Normal" translate="true">,</text><text styleclass="Normal" translate="true">&#32;</text><link displaytype="text" defaultstyle="true" type="topiclink" href="notificationDesigner" anchor="_Notification_designer" styleclass="Normal" translate="true">Notification Designer</link><text styleclass="Normal" translate="true">,</text><text styleclass="Normal" translate="true">&#32;</text><link displaytype="text" defaultstyle="true" type="topiclink" href="ruleSetup" anchor="_Gestione_oggetti_del" styleclass="Normal" translate="true">Rule setup</link><text styleclass="Normal" translate="true">,)</text></para>
        </td>
      </tr>
      <tr style="vertical-align:top">
        <td style="width:157px; height:63px;">
          <para styleclass="Normal"><image src="img30.png" scale="100.00%" styleclass="Normal"></image></para>
        </td>
        <td colspan="2" style="vertical-align:middle; width:488px; height:63px;">
          <para styleclass="Normal"><text styleclass="Normal" translate="true">Data Load via Report (</text><link displaytype="text" defaultstyle="true" type="topiclink" href="dataLoadViaReportDlr" anchor="_Data_load_via" styleclass="Normal" translate="true">Data load via Repor</link><link displaytype="text" defaultstyle="true" type="topiclink" href="dataLoadViaReportDlr" anchor="_Data_load_via" styleclass="Normal" translate="true">t</link><text styleclass="Normal" translate="true"> (DLR))</text></para>
        </td>
      </tr>
      <tr style="vertical-align:top">
        <td style="width:157px; height:24px;">
          <para styleclass="Normal"><image src="img31.jpg" scale="100.00%" styleclass="Normal"></image></para>
        </td>
        <td colspan="2" style="vertical-align:middle; width:488px; height:24px;">
          <para styleclass="Normal"><text styleclass="Normal" translate="true">Group management</text><text styleclass="Normal" translate="true">&#32;</text><text styleclass="Normal" translate="true">(see </text><link displaytype="text" defaultstyle="true" type="topiclink" href="groupManagement" anchor="_Group_Management" styleclass="Normal" translate="true">Group management</link><text styleclass="Normal" translate="true">)</text></para>
        </td>
      </tr>
      <tr style="vertical-align:top">
        <td style="width:157px; height:24px;">
          <para styleclass="Normal"><image src="img32.jpg" scale="100.00%" styleclass="Normal"></image></para>
        </td>
        <td colspan="2" style="vertical-align:middle; width:488px; height:24px;">
          <para styleclass="Normal"><text styleclass="Normal" translate="true">User management</text><text styleclass="Normal" translate="true">&#32;</text><text styleclass="Normal" translate="true">(see </text><link displaytype="text" defaultstyle="true" type="topiclink" href="userManagement" anchor="_User_Management_1" styleclass="Normal" translate="true">DAC Users</link><link displaytype="text" defaultstyle="true" type="topiclink" href="userManagement" anchor="_User_Management_1" styleclass="Normal" translate="true">)</link></para>
        </td>
      </tr>
      <tr style="vertical-align:top">
        <td style="width:157px; height:24px;">
          <para styleclass="Normal"><image src="img33.png" scale="100.00%" styleclass="Normal"></image></para>
        </td>
        <td colspan="2" style="vertical-align:middle; width:488px; height:24px;">
          <para styleclass="Normal"><text styleclass="Normal" translate="true">Object Explorer</text><text styleclass="Normal" translate="true">&#32;</text><text styleclass="Normal" translate="true">(see </text><link displaytype="text" defaultstyle="true" type="topiclink" href="ruleExecution" anchor="_Managing_Application_Objects" styleclass="Normal" translate="true">Managing Application Objects</link><text styleclass="Normal" translate="true">)</text></para>
        </td>
      </tr>
      <tr style="vertical-align:top">
        <td style="width:157px; height:24px;">
          <para styleclass="Normal"><image src="img34.jpg" scale="100.00%" styleclass="Normal"></image></para>
        </td>
        <td colspan="2" style="vertical-align:middle; width:488px; height:24px;">
          <para styleclass="Normal"><text styleclass="Normal" translate="true">Online Help Search Box</text><text styleclass="Normal" translate="true"> (</text><text styleclass="Normal" translate="true">see</text><text styleclass="Normal" translate="true"> Online </text><link displaytype="text" defaultstyle="true" type="topiclink" href="onlineHelp" anchor="_Online_Help" styleclass="Normal" translate="true">Help</link><text styleclass="Normal" translate="true">)</text></para>
        </td>
      </tr>
      <tr style="vertical-align:top">
        <td style="width:157px; height:24px;">
          <para styleclass="Normal"><image src="img35.png" scale="100.00%" styleclass="Normal"></image></para>
        </td>
        <td colspan="2" style="vertical-align:middle; width:488px; height:24px;">
          <para styleclass="Normal"><text styleclass="Normal" translate="true">Start and Stop DAC</text><text styleclass="Normal" translate="true">&#32;</text><text styleclass="Normal" translate="true">(see </text><link displaytype="text" defaultstyle="true" type="topiclink" href="startAndOpenDacLocal" anchor="_Start_RunTime" styleclass="Normal" translate="true">Start DAC</link><text styleclass="Normal" translate="true">)</text></para>
        </td>
      </tr>
      <tr style="vertical-align:top">
        <td style="width:157px; height:24px;">
          <para styleclass="Normal"><image src="img16.png" scale="100.00%" styleclass="Normal"></image></para>
        </td>
        <td colspan="2" style="vertical-align:middle; width:488px; height:24px;">
          <para styleclass="Normal"><text styleclass="Normal" translate="true">Open DAC on Predix</text><text styleclass="Normal" translate="true">&#32;</text><text styleclass="Normal" translate="true">(</text><text styleclass="Normal" translate="true">see </text><link displaytype="text" defaultstyle="true" type="topiclink" href="openDacOnPredix" anchor="_Open_DAC_on" styleclass="Normal" translate="true">Open DAC on Predix</link><text styleclass="Normal" translate="true">)</text></para>
        </td>
      </tr>
      <tr style="vertical-align:top">
        <td style="width:157px; height:24px;">
          <para styleclass="Normal"><image src="img36.png" scale="100.00%" styleclass="Normal"></image></para>
        </td>
        <td colspan="2" style="vertical-align:middle; width:488px; height:24px;">
          <para styleclass="Normal"><text styleclass="Normal" translate="true">Opens widget designer on DAC</text><text styleclass="Normal" translate="true">&#32;</text><text styleclass="Normal" translate="true">(see </text><link displaytype="text" defaultstyle="true" type="topiclink" href="dacDesignPage" anchor="_DAC_Design_Page" styleclass="Normal" translate="true">DAC Design Page</link><text styleclass="Normal" translate="true">)</text></para>
        </td>
      </tr>
    </table></para>
    <para styleclass="Normal"></para>
    <para styleclass="Normal"></para>
  </body>
</topic>
