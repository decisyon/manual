﻿<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="../helpproject.xsl" ?>
<topic template="Default" lasteditedby="sdifranc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../helpproject.xsd">
  <title translate="true">Content Assist filter Conditions - Design Studio Manual</title>
  <body>
    <header>
      <para styleclass="Heading1"><text styleclass="Heading1" translate="true">Content Assist filter Conditions</text></para>
    </header>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">When defining the filter conditions for the Page components (</text><text styleclass="Normal" translate="true">filter</text><text styleclass="Normal" translate="true"> property), </text><text styleclass="Normal" translate="true">content assist</text><text styleclass="Normal" translate="true"> is active that suggests possible commands, such as dimension levels list, entering some operations, the DAC tags or user variables.</text></para>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">With an </text><text styleclass="Normal" translate="true">initial mouse</text><text styleclass="Normal" translate="true">&#32;</text><text styleclass="Normal" translate="true">click</text><text styleclass="Normal" translate="true"> on the formula definition area, you activate a menu with the dimensional levels of the application (in alphabetical order).</text></para>
    <para styleclass="Normal"><text styleclass="Normal" translate="true"> &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160;</text><image src="img350.png" scale="100.00%" styleclass="Normal"></image></para>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">&#32;</text></para>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">By selecting the level you enable </text><text styleclass="Normal" translate="true">content</text><text styleclass="Normal" translate="true">&#32;</text><text styleclass="Normal" translate="true">assist </text><text styleclass="Normal" translate="true">for the selection of some special operations:</text></para>
    <list id="52" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Normal" style="font-family:Symbol; font-size:11pt; color:#000000;">
      <li styleclass="Normal"><text styleclass="Normal" translate="true">IN()</text><text styleclass="Normal" translate="true"> for the manual entry of a list of values.</text></li>
    </list>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">If the dimensional level is descriptive, the system enters the commas inside the brackets, otherwise the values will be inserted without any commas.</text></para>
    <para styleclass="Normal"><image src="img351.jpg" scale="100.00%" styleclass="Normal"></image></para>
    <list id="53" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Normal" style="font-family:Symbol; font-size:11pt; color:#000000;">
      <li styleclass="Normal"><text styleclass="Normal" translate="true">IN (…) </text><text styleclass="Normal" translate="true">if</text><text styleclass="Normal" translate="true">&#32;</text><text styleclass="Normal" translate="true">you have selected it, the system displays the list of values for the selected level in a new window in multi selection.</text></li>
    </list>
    <para styleclass="Normal"><image src="img352.png" scale="100.00%" styleclass="Normal"></image></para>
    <list id="54" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Normal" style="font-family:Symbol; font-size:11pt; color:#000000;">
      <li styleclass="Normal"><text styleclass="Normal" translate="true">IN([?</text><text styleclass="Normal" translate="true">NAME_LEVEL?]</text><text styleclass="Normal" translate="true">)</text><text styleclass="Normal" translate="true"> for the level chosen a filter condition is proposed on the same level, indicated as a parameter.</text></li>
    </list>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">This way, as soon as you export the selected level (e.g. YEAR) to an object of the Page, the report will be filtered by it.</text></para>
    <para styleclass="Normal"><image src="img353.png" scale="100.00%" styleclass="Normal"></image></para>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">&#32;</text></para>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">&#32;</text></para>
    <list id="55" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Normal" style="font-family:Symbol; font-size:11pt; color:#000000;">
      <li styleclass="Normal"><text styleclass="Normal" translate="true">IN([?</text><text styleclass="Normal" translate="true">NAME_PARAMETER?])</text><text styleclass="Normal" translate="true"> ) </text><text styleclass="Normal" translate="true">for each Page parameter, the corresponding filter condition on it is proposed.</text></li>
    </list>
    <para styleclass="Normal_msonormal" style="text-align:justify; text-indent:0px; margin-top:14px; margin-right:0px; margin-bottom:14px; margin-left:0px; line-height:1.19; background-color:transparent; white-space:normal; page-break-inside:auto; page-break-after:auto; border:none; tabstops:none;"><image src="img354.png" scale="100.00%" styleclass="Image Caption"></image></para>
    <para styleclass="Normal_msonormal" style="text-align:justify; text-indent:0px; margin-top:14px; margin-right:0px; margin-bottom:0px; margin-left:0px; line-height:1.19; background-color:transparent; white-space:normal; page-break-inside:auto; page-break-after:auto; border:none; tabstops:none;"><text styleclass="Normal_msonormal" style="font-family:&apos;Times New Roman&apos;; font-size:12pt; font-weight:normal; font-style:normal; text-decoration:none; text-transform:none; vertical-align:baseline; color:#666666; background-color:transparent; letter-spacing:normal; letter-scaling:100%;" translate="true">&#32;</text></para>
  </body>
</topic>
