﻿<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="../helpproject.xsl" ?>
<topic template="Default" lasteditedby="sdifranc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../helpproject.xsd">
  <title translate="true">Starting a Query - Design Studio Manual</title>
  <body>
    <header>
      <para styleclass="Heading1"><text styleclass="Heading1" translate="true">Starting a Query - Design Studio Manual</text></para>
    </header>
    <para styleclass="Normal"></para>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">Starting a query is set by selecting </text><text styleclass="Normal" translate="true">QUERY</text><text styleclass="Normal" translate="true"> in the </text><text styleclass="Normal" translate="true">execution-type</text><text styleclass="Normal" translate="true"> property: the </text><text styleclass="Normal" translate="true">sql-formula</text><text styleclass="Normal" translate="true"> property is enabled to define the query.</text></para>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">The query can either be added or updated, allowing you to interact with the data warehouse or other databases.</text></para>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">The query statement allows the use of parameters. In this case, using the property:</text></para>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">·</text><text styleclass="Normal" translate="true"> &#160; &#160; &#160; &#160; </text><text styleclass="Normal" translate="true">mandatory-params</text><text styleclass="Normal" translate="true"> you can define if the parameters are mandatory (property selected, default) or not (property unselected).</text></para>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">Note: </text><text styleclass="Normal" translate="true">If, the mandatory parameters are not valued before clicking on Execute query, the system shows the fields, and signals an error message.</text></para>
    <para styleclass="Normal"><image src="img676.png" scale="100.00%" styleclass="Normal"></image></para>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">If the application interacts with databases</text><text styleclass="Normal" translate="true"> other than the data warehouse (set in the application), initially a data source is defined for the connection to the database, using the </text><text styleclass="Normal" translate="true">Datasource Designer</text><text styleclass="Normal" translate="true"> module (see </text><text styleclass="Normal" translate="true">Connections to remote data sources section</text><text styleclass="Normal" translate="true">).</text></para>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">Then the data source is associated with the component:</text></para>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">·</text><text styleclass="Normal" translate="true"> &#160; &#160; &#160; &#160; </text><text styleclass="Normal" translate="true">the </text><text styleclass="Normal" translate="true">Custom-datasource</text><text styleclass="Normal" translate="true"> property is selected</text></para>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">·</text><text styleclass="Normal" translate="true"> &#160; &#160; &#160; &#160; </text><text styleclass="Normal" translate="true">the </text><text styleclass="Normal" translate="true">datasource</text><text styleclass="Normal" translate="true"> property is enabled</text><text styleclass="Normal" translate="true">, with which the previously created datasource can be selected</text></para>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">Also see Appendix I for more details on the settings of the SQL statement and the reference to </text><link displaytype="text" defaultstyle="true" type="topiclink" href="formulatingQueries" anchor="_Formulating_queries" styleclass="Normal" translate="true">the data source</link><text styleclass="Normal" translate="true">.</text></para>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">&#32;</text></para>
  </body>
</topic>
