﻿<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="../helpproject.xsl" ?>
<topic template="Default" lasteditedby="sdifranc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../helpproject.xsd">
  <title translate="true">Crosstabs - Design Studio Manual</title>
  <body>
    <header>
      <para styleclass="Heading1"><text styleclass="Heading1" translate="true">Crosstabs - Design Studio Manual</text></para>
    </header>
    <para styleclass="Normal_fill" style="text-align:left; text-indent:0px; margin-top:0px; margin-right:0px; margin-bottom:0px; margin-left:0px; line-height:1.00; background-color:transparent; white-space:normal; page-break-inside:auto; page-break-after:auto; border:none; tabstops:none;"></para>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">The Crosstab component inherits the properties defined in the report; those that can be changed in the component are those relating to the </text><text styleclass="Normal" translate="true">ObjectStyle </text><text styleclass="Normal" translate="true">group</text><text styleclass="Normal" translate="true">.</text></para>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">The properties which can be customized on the Crosstab component in the Page are:</text></para>
    <list id="32" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Normal" style="margin-left:25px; font-family:Symbol; font-size:11pt; color:#000000;">
      <li styleclass="Normal" style="margin-left:25px;"><text styleclass="Normal" translate="true">show-page-by </text><text styleclass="Normal" translate="true">enables the header of the report</text></li>
      <li styleclass="Normal" style="margin-left:25px;"><text styleclass="Normal" translate="true">show-body</text><text styleclass="Normal" translate="true"> enables the display of the content of the report (page-by is excluded)</text></li>
      <li styleclass="Normal" style="margin-left:25px;"><text styleclass="Normal" translate="true">rowsLocked: </text><text styleclass="Normal" translate="true">locks the rows of the report while scrolling</text></li>
      <li styleclass="Normal" style="margin-left:25px;"><text styleclass="Normal" translate="true">colsLocked: </text><text styleclass="Normal" translate="true">locks the columns of the report while scrolling</text></li>
      <li styleclass="Normal" style="margin-left:25px;"><text styleclass="Normal" translate="true">object-</text><text styleclass="Normal" translate="true">transparency defines the transparent color for the cells of the crosstab</text></li>
    </list>
    <para styleclass="Normal" style="margin-left:47px;"><text styleclass="Normal" translate="true">–</text><text styleclass="Normal" translate="true"> &#160; &#160; &#160; &#160;</text><text styleclass="Normal" translate="true">&lt; none &gt; </text><text styleclass="Normal" translate="true">no transparency</text></para>
    <para styleclass="Normal" style="margin-left:47px;"><text styleclass="Normal" translate="true">–</text><text styleclass="Normal" translate="true"> &#160; &#160; &#160; &#160;</text><text styleclass="Normal" translate="true">&lt; body &gt; </text><text styleclass="Normal" translate="true">transparency only for the cells of the body of the report</text></para>
    <para styleclass="Normal" style="margin-left:47px;"><text styleclass="Normal" translate="true">–</text><text styleclass="Normal" translate="true"> &#160; &#160; &#160; &#160;</text><text styleclass="Normal" translate="true">&lt; body &amp; header &gt;</text><text styleclass="Normal" translate="true"> transparency for the body of the report and the headers</text></para>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">The property is applicable and visible only if the row and column lock properties are not enabled.</text></para>
    <list id="33" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Normal" style="margin-left:25px; font-family:Symbol; font-size:11pt; color:#000000;">
      <li styleclass="Normal" style="margin-left:25px;"><text styleclass="Normal" translate="true">force-auto-expand-height (Group Page PDF Export)</text><text styleclass="Normal" translate="true">&#32;</text><text styleclass="Normal" translate="true">sets the auto-expand value in the container during exportation of the PDF Page, so that all the values in the crosstab are printed.</text></li>
      <li styleclass="Normal" style="margin-left:25px;"><text styleclass="Normal" translate="true">show-rows-cols-numbers:</text><text styleclass="Normal" translate="true"> enables the display of the number of rows and columns of the report. The information is visible in the toolbar of the report. Therefore the </text><text styleclass="Normal" translate="true">show-toolbar</text><text styleclass="Normal" translate="true"> property must be enabled.</text></li>
    </list>
    <para styleclass="Normal" style="margin-left:25px;"></para>
  </body>
</topic>
