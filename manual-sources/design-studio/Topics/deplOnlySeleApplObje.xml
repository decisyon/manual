﻿<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="../helpproject.xsl" ?>
<topic template="Default" lasteditedby="sdifranc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../helpproject.xsd">
  <title translate="true">Deploy Only Selected Application Objects</title>
  <body>
    <header>
      <para styleclass="Heading1"><text styleclass="Heading1" translate="true">Deploy Only Selected Application Objects</text></para>
    </header>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">The second step after selecting the Deploy only selected application objects option is the selection of the component to be deployed in Predix.</text></para>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">&#32;</text></para>
    <para styleclass="Normal"><image src="img57.png" scale="100.00%" styleclass="Normal"></image></para>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">&#32;</text></para>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">When you select an individual object, a summary of the features will appear in the section on the right. These relate to the source Data Model and target Data Model (if the object is also present in the latter). The last pane shows the type of operation to be performed on it.</text></para>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">Note that each operation is marked by a different color, which is used by the system to highlight the names of the objects involved.</text></para>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">On the first occasion, they can only be copied, while other operations will also be possible on subsequent occurrences:</text></para>
    <list id="1" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Normal" style="font-family:Symbol; font-size:11pt; color:#000000;">
      <li styleclass="Normal"><text styleclass="Normal" style="font-weight:bold;" translate="true">Creation</text><text styleclass="Normal" translate="true"> of the object in the target Data Model (name highlighted in green), when a new object has been created in the source Data Model</text></li>
      <li styleclass="Normal"><text styleclass="Normal" style="font-weight:bold;" translate="true">Deletion</text><text styleclass="Normal" translate="true"> of the object from the target Data Model (name highlighted in purple), when an object still present in the target Data Model has been deleted in the source Data Model</text></li>
      <li styleclass="Normal"><text styleclass="Normal" style="font-weight:bold;" translate="true">Update</text><text styleclass="Normal" translate="true"> of the object in the target Data Model (name highlighted in blue), when the object has been modified in the source Data Model, while it has not underwent any changes in the destination Data Model</text></li>
      <li styleclass="Normal"><text styleclass="Normal" style="font-weight:bold;" translate="true">Conflicts Between</text><text styleclass="Normal" translate="true"> the characteristics of the object in the source and target Data Model (name highlighted in red), when the object has been modified in both the source and target Data Model. The conflict is resolved by applying the changes made to the source object to the target object</text></li>
      <li styleclass="Normal"><text styleclass="Normal" style="font-weight:bold;" translate="true">Restoring</text><text styleclass="Normal" translate="true"> the object in the target Data Model (name highlighted in orange), when an object still in the source Data Model has been deleted in the target Data Model</text></li>
    </list>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">When you select the items you want and click on the Next button, you display a summary window with the objects selected previously.</text></para>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">In some cases, the advanced operations column shows certain advanced operations for a specific object. The next step (Next button) launches the deploy activity.</text></para>
    <para styleclass="Normal"><image src="img58.jpg" scale="100.00%" styleclass="Normal"></image></para>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">The deploy tool can perform the following operations:</text></para>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">• &#160; &#160; &#160;</text><text styleclass="Normal" style="font-weight:bold;" translate="true"> Add (green):</text><text styleclass="Normal" translate="true"> create the object in the target environment.</text></para>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">• &#160; &#160; &#160; </text><text styleclass="Normal" style="font-weight:bold;" translate="true">Delete (brown):</text><text styleclass="Normal" translate="true"> delete the object in the target environment, since the same object has been deleted in the source environment.</text></para>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">• &#160; &#160; &#160; </text><text styleclass="Normal" style="font-weight:bold;" translate="true">Modify (blue): </text><text styleclass="Normal" translate="true">update the object in the target environment, since the same object has been modified in the source environment.</text></para>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">• &#160; &#160; &#160; </text><text styleclass="Normal" style="font-weight:bold;" translate="true">Conflict (red):</text><text styleclass="Normal" translate="true"> the same object has been modified in both source and target environments. The conflict is resolved by modifying the object in the target environment.</text></para>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">• &#160; &#160; &#160; </text><text styleclass="Normal" style="font-weight:bold;" translate="true">Restore Add (orange):</text><text styleclass="Normal" translate="true"> restore the object that has been deleted in the target environment but still exists in the source environment.</text></para>
    <para styleclass="Normal_body-tool" style="text-align:right; text-indent:0px; margin-top:14px; margin-right:0px; margin-bottom:0px; margin-left:0px; line-height:1.00; background-color:transparent; white-space:normal; page-break-inside:auto; page-break-after:auto; border:none; tabstops:none;"></para>
  </body>
</topic>
