﻿<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="../helpproject.xsl" ?>
<topic template="Default" lasteditedby="sdifranc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../helpproject.xsd">
  <title translate="true">Contextuality in Pages: Parameters to Link Widget </title>
  <body>
    <header>
      <para styleclass="Heading1"><text styleclass="Heading1" translate="true">Contextuality in Pages: Parameters to Link Widget</text></para>
    </header>
    <para styleclass="Normal_fill" style="text-align:left; text-indent:0px; margin-top:0px; margin-right:0px; margin-bottom:0px; margin-left:0px; line-height:1.00; background-color:transparent; white-space:normal; page-break-inside:auto; page-break-after:auto; border:none; tabstops:none;"></para>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">Most of the widgets available in DAC support both parameter import and export: they can create and assign value to a new parameter or they can import and leverage the value of an existing page parameter. </text></para>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">Some of the widgets automatically create a parameter value while in others you need to configure the export of parameters. </text></para>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">Some of the widgets that automatically create parameters are:</text></para>
    <list id="2" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Normal" style="font-family:Symbol; font-size:11pt; color:#000000;">
      <li styleclass="Normal"><text styleclass="Normal" translate="true">Predix / Asset Model</text></li>
      <li styleclass="Normal"><text styleclass="Normal" translate="true">Predix / Dropdown</text></li>
      <li styleclass="Normal"><text styleclass="Normal" translate="true">Predix / Slider</text></li>
      <li styleclass="Normal"><text styleclass="Normal" translate="true">Predix / Range Picker</text></li>
      <li styleclass="Normal"><text styleclass="Normal" translate="true">Predix / Datetime Picker</text></li>
      <li styleclass="Normal"><text styleclass="Normal" translate="true">Predix / Datetime Range Panel</text></li>
      <li styleclass="Normal"><text styleclass="Normal" translate="true">Predix / Typehead</text></li>
      <li styleclass="Normal"><text styleclass="Normal" translate="true">Form / Styled Selector</text></li>
      <li styleclass="Normal"><text styleclass="Normal" translate="true">Form / Plain Text</text></li>
      <li styleclass="Normal"><text styleclass="Normal" translate="true">Form / Text Area</text></li>
      <li styleclass="Normal"><text styleclass="Normal" translate="true">Button / Execute button</text></li>
    </list>
    <para styleclass="Normal"></para>
    <para styleclass="Normal"><text styleclass="Normal" translate="true">Some of the widgets that you need to configure to export parameters are:</text></para>
    <list id="3" type="ul" listtype="bullet" formatstring="&#183;" format-charset="SYMBOL_CHARSET" levelreset="true" legalstyle="false" startfrom="1" styleclass="Normal" style="font-family:Symbol; font-size:11pt; color:#000000;">
      <li styleclass="Normal"><text styleclass="Normal" translate="true">Data Visualization / Crosstab</text></li>
      <li styleclass="Normal"><text styleclass="Normal" translate="true">Data Visualization / Chart</text></li>
      <li styleclass="Normal"><text styleclass="Normal" translate="true">Data Visualization / Map</text></li>
      <li styleclass="Normal"><text styleclass="Normal" translate="true">Resources / JavaScript</text></li>
    </list>
    <para styleclass="Normal"></para>
    <para styleclass="Title Example"><text styleclass="Title Example" translate="true">Example: Widget that Automatically Create a Parameter</text></para>
    <para styleclass="Normal_msonormal" style="text-align:justify; text-indent:0px; margin-top:14px; margin-right:0px; margin-bottom:0px; margin-left:0px; line-height:1.19; background-color:transparent; white-space:normal; page-break-inside:auto; page-break-after:auto; border:none; tabstops:none;"><image src="img344.jpg" scale="100.00%" styleclass="Image Caption"></image></para>
    <para styleclass="Normal_msonormal" style="text-align:justify; text-indent:0px; margin-top:14px; margin-right:0px; margin-bottom:0px; margin-left:0px; line-height:1.19; background-color:transparent; white-space:normal; page-break-inside:auto; page-break-after:auto; border:none; tabstops:none;"></para>
    <para styleclass="Text Example"><text styleclass="Text Example" translate="true"> The example shows inserting a widget into the page that automatically creates a parameter. Note: you are required to insert a name for the parameter. The name of the parameter is also visible in the widget’s box in Page Design.</text></para>
    <para styleclass="Normal_msonormal" style="text-align:justify; text-indent:0px; margin-top:14px; margin-right:0px; margin-bottom:0px; margin-left:0px; line-height:1.38; background-color:transparent; white-space:normal; page-break-inside:auto; page-break-after:auto; border:none; tabstops:none;"></para>
    <para styleclass="Title Example"><text styleclass="Title Example" translate="true">Example: Widget that you have to configure to export parameters</text></para>
    <para styleclass="Normal_msonormal" style="text-align:justify; text-indent:0px; margin-top:14px; margin-right:0px; margin-bottom:0px; margin-left:0px; line-height:1.19; background-color:transparent; white-space:normal; page-break-inside:auto; page-break-after:auto; border:none; tabstops:none;"><image src="img346.png" scale="100.00%" styleclass="Image Caption"></image></para>
    <para styleclass="Normal_msonormal" style="text-align:justify; text-indent:0px; margin-top:14px; margin-right:0px; margin-bottom:0px; margin-left:0px; line-height:1.19; background-color:transparent; white-space:normal; page-break-inside:auto; page-break-after:auto; border:none; tabstops:none;"></para>
    <para styleclass="Text Example"><text styleclass="Text Example" translate="true">The example shows</text></para>
    <para styleclass="Text Example"><text styleclass="Text Example" translate="true">Inserting a widget into the page that you need to configure to export parameters. When you insert this type of widget into the page, in this example the Crosstab widget, you need to check the property activate-params-export. You can export the parameter as a single value or multiple values.</text></para>
    <para styleclass="Text Example"><text styleclass="Text Example" translate="true">You can now configure</text></para>
    <para styleclass="Text Example"><text styleclass="Text Example" translate="true">1. &#160; &#160; &#160; Which column of the report will be exported as a parameter.</text></para>
    <para styleclass="Text Example"><text styleclass="Text Example" translate="true">2. &#160; &#160; &#160; If the parameter is exported as a single value or multiple value. When using DAC, this changes the behavior of the controller from a single selection to multiple selection (see images).</text></para>
    <para styleclass="Normal_msonormal" style="text-align:justify; text-indent:0px; margin-top:14px; margin-right:0px; margin-bottom:0px; margin-left:0px; line-height:1.00; background-color:transparent; white-space:normal; page-break-inside:auto; page-break-after:auto; border:none; tabstops:none;"><text styleclass="s614" style="font-family:&apos;Times New Roman&apos;; font-size:9pt; font-weight:bold; font-style:normal; text-decoration:none; text-transform:none; vertical-align:baseline; color:#666666; background-color:transparent; letter-spacing:normal; letter-scaling:100%;" translate="true">&#32;</text></para>
  </body>
</topic>
